#ifndef _LIB_DAEMON_H_
#define _LIB_DAEMON_H_

void daemonize(void);
int is_daemon_alive(const char* daemon_name);
int startup_daemon(const char* daemon_name);

#endif
