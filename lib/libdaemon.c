#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include "libdebug.h"
#include "libdaemon.h"

void daemonize(void)
{
    pid_t pid, sid;
    int fd;

	// Already a daemon? Parent process is init.d => parent pid = 1
	if ( getppid() == 1 )
		return;

	switch ( pid = fork() ) {
    case -1: // Error
        P_ERR_STR("Failure to fork - exiting");
        exit(EXIT_FAILURE);
    case 0: // Child process
        break;
    default: // Kills the Parent Process, orphaning this one and making initd adopt it
        exit(EXIT_SUCCESS);
	}

	// Create a new SID for the child process
	sid = setsid();
	if (sid < 0) {
        P_ERR_STR("Failure to setsid - exiting");
		exit(EXIT_FAILURE);
	}

	// change the current working directory
	if ((chdir("/")) < 0)  {
        P_ERR_STR("Failure to chdir - exiting");
		exit(EXIT_FAILURE);
	}

	fd = open("/dev/null",O_RDWR, 0);

	if (fd != -1) {
		dup2(fd, STDIN_FILENO);
		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDERR_FILENO);

		if (fd > 2)
			close (fd);
	}
	else {
		P_ERR_STR("Failure to close STDs - exiting");
		exit(EXIT_FAILURE);
	}

	// Resetting File Creation Mask
	umask(027);
}

int is_daemon_alive(const char* daemon_name)
{
    char* command = NULL;
    size_t size;
    {
        int r = snprintf(command, 0, "pidof %s", daemon_name);
        if(r < 0) {
            P_ERR("snprintf failed, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
        size = (size_t)r;
    }
    command = malloc(sizeof(char) * (size + 1)); // NUL char
    snprintf(command, size + 1, "pidof %s", daemon_name);
    P_INFO("Checking if daemon is alive: `%s`", command);
    return system(command);
}

int startup_daemon(const char* daemon_name)
{
    char* command = NULL;
    size_t size;
    {
        int r = snprintf(command, 0, "/usr/bin/%s", daemon_name);
        if(r < 0) {
            P_ERR("snprintf failed, errno: %d (%s)", errno, strerror(errno));
            return EXIT_FAILURE;
        }
        size = (size_t)r;
    }
    command = malloc(sizeof(char) * (size + 1)); // NUL char
    snprintf(command, size + 1, "/usr/bin/%s", daemon_name);
    P_INFO("Starting up daemon: `%s`", command);
    return system(command);
}

